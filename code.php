<?php

class Building{


	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name,$floors,$address){

		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function getName(){
		return $this->name;
	}
	public function getFloors(){
		return $this->floors;
	}
	public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		if(gettype($name) === "string"){
			 $this->name = $name;
		}
	}
	private function setFloors($floors){
		if(gettype($floors) === "string"){
			 $this->floors = $floors;
		}
	}
	private function setAddress($address){
		if(gettype($address) === "string"){
			 $this->address = $address;
		}
	}


}

class Condominium extends Building{
	


}
$building = new Building('Caswynn Building',8,'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo condo',5, 'Buendia Avenue Makati City, Philippines');
